import { combineReducers } from 'redux'
import {nav} from './dataReducers'
import { reducer as form } from 'redux-form'

const rootReducer = combineReducers({
    nav,
    form
})

export default rootReducer