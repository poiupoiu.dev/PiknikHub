/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';
import {connect} from "react-redux";
import axios from 'axios'
import {Button, Container, Content, Footer, FooterTab, Header, SwipeRow} from "native-base";
import Icon from 'react-native-vector-icons/FontAwesome';

class screen_detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pro_id: null,
            data: []
        }

    }

    componentWillMount() {

        const {params} = this.props.navigation.state;
        this.setState({
            pro_id: params.pro_id
        })

        const config = {
            headers: {'content-type': 'multipart/form-data'}
        };
        const data = new FormData();
        data.append('act', 'list_sb_pro');
        data.append('project_id', params.pro_id);
        axios.post("https://jnck.mlskoding.com/api/subproject", data, config)
            .then(function (response) {
                if (response.data.status === true) {
                    console.log(response.data.data)
                    this.setState({
                        data: response.data.data
                    })
                }
                //console.log(response.data)
                // this.setState({
                //     data:response.data
                // })
            }.bind(this))
            .catch(function (error) {
                console.log(error);
            });
        //console.log(params.pro_id)
    }

    render() {

        return (
            <Container style={{backgroundColor: '#212121'}}>
                <Header style={{backgroundColor: '#03A9F4'}}/>
                <Content>
                    {
                        this.state.data.length > 0
                            ?
                            this.state.data.map((x, i) => {
                                return (
                                    <SwipeRow
                                        style={{marginTop: 10, marginRight: 5, marginLeft: 5}}
                                        key={i}
                                        leftOpenValue={75}
                                        rightOpenValue={-75}
                                        left={
                                            <Button style={{height: 50}} success onPress={() => alert('Add')}>
                                            </Button>
                                        }
                                        body={
                                            <View style={{paddingLeft: 5, height: 50, flex: 1}}>
                                                <Text>{x.sb_pro_name}</Text>
                                                <View style={{flex: 1, flexDirection: 'row'}}>
                                                    <View style={{flex: 1}}>
                                                        <Text>Priority : {x.sb_pro_prio}</Text>
                                                    </View>
                                                    <View style={{flex: 1}}>
                                                        <Text>Status : {x.sb_pro_status}</Text>
                                                    </View>
                                                </View>

                                                <View style={{flex: 1, flexDirection: 'row', marginTop: 20}}>
                                                    <View style={{flex: 1}}>
                                                        <Icon size={11} name="circle-o" color="#00E676">
                                                            <Text style={{fontSize: 10}}> {x.sb_pro_create_date}</Text>
                                                        </Icon>
                                                    </View>
                                                    <View style={{flex: 1}}>
                                                        <Icon size={11} name="check" color="#03a9f4">
                                                            <Text style={{fontSize: 10}}> {x.sb_pro_end_date}</Text>
                                                        </Icon>
                                                    </View>
                                                    <View style={{flex: 1}}>
                                                        <Icon size={11} name="times" color="#DD2C00">
                                                            <Text style={{fontSize: 10}}> {x.sb_dead_line}</Text>

                                                        </Icon>
                                                    </View>
                                                </View>
                                            </View>
                                        }
                                        right={
                                            <Button style={{marginRight: 5, height: 50}} info
                                                    onPress={() => alert('Trash')}>
                                            </Button>
                                        }
                                    />
                                )
                            })

                            :
                            null
                    }

                </Content>
                <Footer style={{backgroundColor: '#212121', margin: 5}}>
                    <FooterTab style={{backgroundColor: '#212121'}}>
                        <Button full bordered info onPress={() => this.props.navigation.navigate('AddDet')}>
                            <Text>Add</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default connect()(screen_detail)
