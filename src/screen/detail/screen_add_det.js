/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text, TouchableWithoutFeedback,
    View
} from 'react-native';
import {connect} from "react-redux";
import {Button, Container, Content, Footer, FooterTab, Header, Input, Item} from "native-base";
import { RadioButtons } from 'react-native-radio-buttons'
import DatePicker from 'react-native-datepicker'

class screen_add_det extends Component {
    constructor(){
        super();
        this.state = {

            selectedOption:[],
            selectedIndex: 0,
        }

    }
    // componentDidMount(){
    //     setTimeout(() => {
    //         this.props.navigation.dispatch({type: 'Home'});
    //     }, 1000)
    // }
    render() {
        const options = [
            "Very Low",
            "Low",
            "High",
            "Very High"
        ];
        function setSelectedOption(selectedOption,selectedIndex){
            this.setState({
                selectedOption
            });
            this.setState({
                selectedIndex
            });
        }
        function renderOption(option, selected, onSelect, index){
            const style = selected ? { fontWeight: 'bold',fontSize:14, color:'#000'} : {fontSize:14, color:'#000'};

            return (
                <TouchableWithoutFeedback onPress={onSelect} key={index}>
                    <View>
                    <Text style={style}>{option}</Text>
                    </View>
                </TouchableWithoutFeedback>
            );
        }
        let today = new Date();
        let minDate=(today.getFullYear()-90) + "-"+ parseInt(today.getMonth()) +"-"+ today.getDate();
        let maxDate=today.getFullYear()+1+"-"+ parseInt(today.getMonth()) +"-"+ today.getDate();
        return (
            <Container style={{backgroundColor: '#212121'}}>
                <Header style={{backgroundColor: '#03A9F4'}}/>
                <Content>
                    <View style={{margin:5,backgroundColor:'#fff'}}>

                    <Item>
                        <Input style={{backgroundColor:'#fff', fontSize:14, height:40}} placeholder='To Do List Name' />
                    </Item>
                    </View>
                    <View style={{margin:5,backgroundColor:'#fff'}}>

                        <Item>
                            <Input multiline={true}
                                   numberOfLines={4}
                                   style={{backgroundColor:'#fff', fontSize:14}} placeholder='Note' />
                        </Item>
                    </View>
                    <View style={{margin:5}}>
                        <View style={{height:40,backgroundColor:'#fff', justifyContent:'center'}}>
                        <RadioButtons
                            options={ options }
                            onSelection={ setSelectedOption.bind(this) }
                            selectedOption={this.state.selectedOption }
                            renderOption={ renderOption }
                            selectedIndex={this.state.selectedIndex}
                            renderContainer={RadioButtons.getViewContainerRenderer({
                                flexDirection: 'row',
                                justifyContent: 'space-around',
                            })}
                        />
                        </View>
                    </View>
                    <View style={{margin:5,backgroundColor:'#fff', alignItems:'center'}}>

                        <Item>
                            <Text>Dead Line</Text>
                            <DatePicker
                                style={{width: 200}}
                                date={this.state.date}
                                mode="date"
                                placeholder="select date"
                                format="YYYY-MM-DD"
                                minDate={minDate}
                                maxDate={maxDate}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                showIcon={false}
                                customStyles={{
                                    dateInput: {
                                        borderWidth:0,
                                    }
                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(date) => {this.setState({date: date})}}
                            />
                        </Item>
                    </View>
                </Content>
                <Footer style={{backgroundColor: '#212121', margin:5}}>
                    <FooterTab style={{backgroundColor: '#212121'}}>
                        <Button full bordered info>
                            <Text>Save</Text>
                        </Button>
                    </FooterTab>
                </Footer>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default connect()(screen_add_det)
