/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry, Image, ImageBackground,
    StyleSheet,
    View
} from 'react-native';
import {connect} from "react-redux";
import Icon from 'react-native-vector-icons/FontAwesome';
import {ListView, TouchableWithoutFeedback} from 'react-native';
import {Button, Container, Content, Footer, FooterTab, Form, Header, Input, Item, SwipeRow, Text} from "native-base";
import axios from 'axios'
const plesir = [
    {
        image : require('../../assets/panorama/indrayanti.png'),
        destination : 'Pantai Indrayanti, DI Yogykarta, Indonesia',
        date : '1 Desember 2017',
        hours : '10:00',
        meet_poing : 'Sleman, Yogyakarta',
        capacity : 10,
        val : 5
    },
    {
        image : require('../../assets/panorama/jungwok.png'),
        destination : 'Pantai Jungwok, DI Yogykarta, Indonesia',
        date : '1 Desember 2017',
        hours : '10:00',
        meet_poing : 'Sleman, Yogyakarta',
        capacity : 10,
        val : 5
    },
    {
        image : require('../../assets/panorama/indrayanti.png'),
        destination : 'Pantai Indrayanti, DI Yogykarta, Indonesia',
        date : '1 Desember 2017',
        hours : '10:00',
        meet_poing : 'Sleman, Yogyakarta',
        capacity : 10,
        val : 5
    }
    ,
    {
        image : require('../../assets/panorama/merbabu.png'),
        destination : 'Gunung Merbabu, DI Yogykarta, Indonesia',
        date : '1 Desember 2017',
        hours : '10:00',
        meet_poing : 'Sleman, Yogyakarta',
        capacity : 10,
        val : 5
    }
    ,
    {
        image : require('../../assets/panorama/andong.png'),
        destination : 'Gunung Andong, DI Yogykarta, Indonesia',
        date : '1 Desember 2017',
        hours : '10:00',
        meet_poing : 'Sleman, Yogyakarta',
        capacity : 10,
        val : 5
    }
]
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            int: 0
        }

        this.onRedirect = this.onRedirect.bind(this)

    }

    componentWillMount() {

    }

    onRedirect(){
        this.props.navigation.dispatch({type: 'Navigation/BACK'})
    }

    render() {
        //const { navigate } = this.props.navigation;
        //console.log(this.state.data)
        return (
            <View style={{padding:10}}>

                {
                    plesir.map((x,i)=>{
                        return (
                            <TouchableWithoutFeedback key={i} onPress={this.onRedirect}>
                                <View style={{height: 125, marginTop:5, marginBottom:5}}>
                                    <View style={{flex: 1}}>
                                        <ImageBackground resizeMode="contain" source={x.image}
                                                         style={{
                                                             flex: 1,
                                                             alignSelf: 'stretch',
                                                             width: undefined,
                                                             height: undefined,
                                                             flexWrap: 'wrap',
                                                             flexDirection: 'column',
                                                         }}>
                                            <View style={{flex: 2, backgroundColor: 'rgba(52, 52, 52, 0.3)'}}>

                                            </View>
                                            <View style={{flex: 2, backgroundColor: 'rgba(52, 52, 52, 0.6)'}}>
                                                <Text style={{fontSize:10, color:'#FFFFFF', textAlign:'right', paddingRight:10}}><Icon color={'#FFFFFF'} name="location-arrow" size={13} />{'  '}{x.destination}</Text>
                                                <Text style={{marginTop:5,fontSize:10, color:'#FFFFFF', textAlign:'right', paddingRight:10}}><Icon color={'#FFFFFF'} name="calendar-check-o" size={12} />{'  '}{x.date}{'        '}<Icon color={'#FFFFFF'} name="clock-o" size={12} />{'  '}{x.hours}</Text>

                                            </View>
                                            <View style={{flex: 3, backgroundColor: "'rgba(255, 255, 255, 1)'"}}>
                                                <View style={{flex:1, flexDirection:'column', paddingLeft:15, paddingTop:7, }}>
                                                    <View style={{flex:1}}>
                                                        <Text style={{fontSize:10}}><Icon color={'#424242'} name="map-marker" size={13} />{'    '}Dari {x.meet_poing}</Text>
                                                    </View>
                                                    <View style={{flex:1, paddingBottom:4}}>
                                                        <Text style={{fontSize:10}}><Icon color={'#424242'} name="user-o" size={13} />{'   '}{x.val} dari {x.capacity} orang</Text>
                                                    </View>
                                                </View>

                                            </View>
                                        </ImageBackground>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        )
                    })
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default connect()(Home)
