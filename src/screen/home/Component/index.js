import Home from './Home'
import Location from './Location'
import Pesan from './Pesan'
import Profile from './Profile'
import Detail from './Detail'

export {
    Home,
    Location,
    Pesan,
    Profile,
    Detail
}