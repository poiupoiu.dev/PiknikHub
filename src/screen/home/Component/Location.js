/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View
} from 'react-native';
import {connect} from "react-redux";
import {ListView, TouchableWithoutFeedback} from 'react-native';
import {Button, Container, Content, Footer, FooterTab, Form, Header, Input, Item, SwipeRow, Text} from "native-base";
import axios from 'axios'
import MapView from 'react-native-maps';
const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        height: 400,
        width: 400,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});

class Location extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            int: 0
        }

    }

    componentWillMount() {

    }

    render() {
        //console.log(this.state.data)
        return (
            <View style ={styles.container}>
                <MapView
                    style={styles.map}
                    region={{
                        latitude: 37.78825,
                        longitude: -122.4324,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                >
                </MapView>
            </View>
        );
    }
}


export default connect()(Location)
