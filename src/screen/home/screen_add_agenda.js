/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View, Dimensions
} from 'react-native';
import {connect} from "react-redux";
import {ListView, TouchableWithoutFeedback} from 'react-native';
import {
    Body, Button, Container, Content, Footer, FooterTab, Form, Header, Input, Item, Left, Picker, Right, SwipeRow,
    Text, Title
} from "native-base";
import axios from 'axios'
import color from '../../utils/Constant/Color/Style'
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView from 'react-native-maps';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

const homePlace = {description: 'Home', geometry: {location: {lat: 48.8152937, lng: 2.4597668}}};
const workPlace = {description: 'Work', geometry: {location: {lat: 48.8496818, lng: 2.2940881}}};
var {width, height} = Dimensions.get('window')
import Modal from 'react-native-modal'
import DatePicker from 'react-native-datepicker'

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        width: width,
        height: height
    },
});

class screen_add_agenda extends Component {
    constructor(props) {
        super(props);
        this.state = {
            biaya:0,
            kuota:0,
            desc:'',
            data: [],
            int: 0,
            latitude: -7.779746,
            longitude: 110.3779918,
            isModalVisible: false,
            isLocation: false,
            mp: '',
            canada: '',
            destination: 0,
            datetime1: '20:00',
            date: '2017-11-26'
        }

    }

    _getOptionList() {
        return this.refs['OPTIONLIST'];
    }


    _canada(province) {

        this.setState({
            ...this.state,
            canada: province
        });
    }


    _showModal() {
        this.setState({isModalVisible: true})
    }

    _hideModal() {
        this.setState({isModalVisible: false})
    }

    setLocation() {
        this.setState({isLocation: true})
    }

    unSetLocation() {
        this.setState({isLocation: false})
    }

    componentWillMount() {

    }

    onSave() {
        const params = {
            wisata_id: this.state.destination,
            agenda_create_by: 1,
            agenda_date: this.state.date,
            agenda_time: this.state.datetime1,
            meeting_point_address: this.state.mp,
            meeting_point_lang: this.state.longitude,
            meeting_point_lat: this.state.latitude,
            agenda_kuota: this.state.kuota,
            agenda_biaya: this.state.biaya,
            agenda_desc: this.state.desc,
            agenda_facility: 'xxx',
            id_jenis_kendaraan: 1,
        }

        console.log(params)
    }

    render() {
        //console.log(this.state.data)
        return (
            <Container>
                <Header style={color.headerLognRegis}>
                    <Left style={{flex: 1}}>
                        <Button full transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon size={20} name="arrow-left" color="#FFFFFF"/>
                        </Button>
                    </Left>
                    <Body style={{flex: 4}}>
                    <Title style={{fontSize: 14}}>Tambah Agenda</Title>
                    </Body>
                    <Right style={{flex: 1}}>
                        <Button transparent>
                            <Icon size={20} name="search" color="#FFFFFF" onPress={() => {
                                this._showModal();
                                this.unSetLocation()
                            }}/>
                        </Button>
                    </Right>
                </Header>
                <Modal isVisible={this.state.isModalVisible}>
                    <View style={{flex: 1}}>
                        <GooglePlacesAutocomplete
                            placeholder='Search'
                            minLength={2} // minimum length of text to search
                            autoFocus={false}
                            fetchDetails={true}
                            onChangeText={endGoogle => console.log(endGoogle)}
                            onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                                console.log(data.description);
                                console.log(details.geometry);
                                this.setState({
                                    isModalVisible: false,
                                    latitude: details.geometry.location.lat,
                                    longitude: details.geometry.location.lng,
                                    mp: data.description
                                })
                            }}
                            getDefaultValue={() => {
                                return ''; // text input default value
                            }}
                            query={{
                                // available options: https://developers.google.com/places/web-service/autocomplete
                                key: 'AIzaSyDxOSaYjo0G8_9t57beEzXwijTFBY0gXLY',
                                language: 'en', // language of the results
                                types: '(cities)', // default: 'geocode'
                            }}
                            styles={{
                                textInputContainer: {
                                    backgroundColor: 'rgba(0,0,0,0)',
                                    borderTopWidth: 0,
                                    borderBottomWidth: 0
                                },
                                textInput: {
                                    marginLeft: 0,
                                    marginRight: 0,
                                    height: 38,
                                    color: '#000',
                                    fontSize: 16
                                },
                                predefinedPlacesDescription: {
                                    color: '#000000'
                                },
                                listView: {
                                    backgroundColor: '#fff'
                                }
                            }}

                            currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
                            currentLocationLabel="Current location"
                            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
                            GoogleReverseGeocodingQuery={{
                                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
                            }}
                            GooglePlacesSearchQuery={{
                                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                                rankby: 'distance',
                                types: 'geocode',
                            }}


                            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3', 'country']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities


                            predefinedPlacesAlwaysVisible={true}
                        />
                    </View>
                </Modal>
                <Content style={{backgroundColor: '#FFF'}}>
                    {
                        this.state.isLocation
                            ?
                            <View>
                                <View style={{padding: 10}}>
                                    <Picker
                                        selectedValue={this.state.destination}
                                        onValueChange={(lang) => this.setState({destination: lang})}>
                                        <Picker.Item label="Indrayanti" value={1}/>
                                        <Picker.Item label="Merbabu" value={2}/>
                                        <Picker.Item label="Sadranan" value={3}/>
                                        <Picker.Item label="Andong" value={4}/>
                                        <Picker.Item label="Jungwok" value={5}/>
                                    </Picker>
                                </View>
                                <View style={{padding: 10, flex: 1, flexDirection: 'row'}}>
                                    <View style={{flex: 1}}>
                                        <DatePicker
                                            style={{width: 200}}
                                            date={this.state.date}
                                            mode="date"
                                            placeholder="placeholder"
                                            format="YYYY-MM-DD"
                                            minDate="2016-05-01"
                                            maxDate="2016-06-01"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            onDateChange={(date) => {
                                                this.setState({date: date});
                                            }}
                                        />
                                    </View>
                                    <View style={{flex: 1}}>
                                        <DatePicker
                                            style={{width: 200}}
                                            date={this.state.datetime1}
                                            mode="time"
                                            format="HH:mm"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 4,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 36
                                                }
                                            }}
                                            minuteInterval={10}
                                            onDateChange={(datetime) => {
                                                this.setState({datetime1: datetime});
                                            }}
                                        />
                                    </View>
                                </View>
                                <View style={{padding: 10}}>
                                    <Item regular>
                                        <Input placeholder='Meeting Point' value={this.state.mp} onFocus={() => {
                                            this._showModal();
                                            this.unSetLocation()
                                        }}/>
                                    </Item>
                                </View>
                                <View style={{padding: 10}}>
                                    <Picker
                                        selectedValue={this.state.destination}
                                        onValueChange={(lang) => this.setState({destination: lang})}>
                                        <Picker.Item label="Bus" value={1}/>
                                        <Picker.Item label="Motor" value={2}/>
                                        <Picker.Item label="Mobil" value={3}/>
                                    </Picker>
                                </View>

                                <View style={{padding: 10}}>
                                    <Item regular>
                                        <Input placeholder='Jumlah kuota' keyboardType="numeric" value={this.state.kuota} onChangeText={(kuota) => this.setState({kuota:kuota})}/>
                                    </Item>
                                </View>
                                <View style={{padding: 10}}>
                                    <Item regular>
                                        <Input placeholder='Biaya patungan / orang' keyboardType="numeric" value={this.state.biaya} onChangeText={(biaya) => this.setState({biaya:biaya})}/>
                                    </Item>
                                </View>
                                <View style={{padding: 10}}>
                                    <Item regular>
                                        <Input placeholder='Keterangan Lain' value={this.state.desc} onChangeText={(desc) => this.setState({desc:desc})}/>
                                    </Item>
                                </View>
                            </View>
                            :
                            <MapView
                                style={styles.map}
                                initialRegion={{
                                    latitude: this.state.latitude,
                                    longitude: this.state.longitude,
                                    latitudeDelta: 0.0922,
                                    longitudeDelta: 0.0421,
                                }}

                            ><MapView.Marker draggable
                                             coordinate={{
                                                 latitude: this.state.latitude,
                                                 longitude: this.state.longitude
                                             }}
                                             onDragEnd={(e) => this.setState({x: e.nativeEvent.coordinate})}
                            />
                            </MapView>
                    }

                </Content>
                <Footer>
                    {
                        !this.state.isLocation
                            ?
                            <FooterTab>
                                <Button onPress={() => this.setLocation()}>
                                    <Text>Plih Lokasi</Text>
                                </Button>
                            </FooterTab>
                            :
                            <FooterTab>
                                <Button onPress={() => this.onSave()}>
                                    <Text>Tambah Agenda</Text>
                                </Button>
                                <Button onPress={() => this.unSetLocation()}>
                                    <Text>Pilih Lokasi</Text>
                                </Button>
                            </FooterTab>
                    }

                </Footer>
            </Container>

        );
    }
}


export default connect()(screen_add_agenda)
