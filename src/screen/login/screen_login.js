/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
    AppRegistry, Image,
    StyleSheet,
    View
} from 'react-native';
import {connect} from "react-redux";
import {ListView, TouchableWithoutFeedback} from 'react-native';
import {
    Body,
    Button, Container, Content, Footer, FooterTab, Form, Header, Input, Item, Left, Right, SwipeRow, Tab, Tabs,
    Text, Title
} from "native-base";
import axios from 'axios'
import color from '../../utils/Constant/Color/Style'
import Icon from 'react-native-vector-icons/FontAwesome';
import * as Animatable from 'react-native-animatable';
import Head from '../../Component/Header'
class screen_login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            int: 0,
            email : '',
            password:'',
            errors_message:[],
            showLoading:false,
            isError:false,
            isLoginFail:false
        }

    }

    componentWillMount() {
        //this.props.navigation.dispatch({type: 'onLoginSuccess'})
    }

    handleInputChange(key){
        return ((e)=>{
            let state = {};
            console.log(e.target)
            state[key] = e.target.value;
            this.setState(state);
        });
    }

    onLogin(){
        let errors = [];

        const params = {
            email : this.state.email,
            password : this.state.password
        }

        if (this.state.email === '') {
            errors.push({message: "email required"});
        }
        if (this.state.password === '') {
            errors.push({message: "password required"});
        }

        if (errors.length === 0) {
            this.setState({
                errors_message: []
            })
            if (params.email==='yosafatbama.arintoko@gmail.com' && params.password==='poiupoiu'){
                console.log("login success");
                this.props.navigation.dispatch({type: 'onLoginSuccess'})
            } else {
                this.setState({
                    showLoading: false,
                    isLoginFail:true
                })
                console.log("login failed")
            }
            console.log(params)
        } else {
            this.setState({
                errors_message: [],
                showLoading: false,
                isError:true
            })
            this.setState({
                errors_message: errors
            })
        }




    }

    render() {
        //console.log(this.state.errors_message.length)
        return (
            <Container style={color.bg}>
                <Header style={color.headerLognRegis} >
                    <Left>
                        <Button full transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon size={20} name="arrow-left" color="#FFFFFF"/>
                        </Button>
                    </Left>
                    <Body>
                    <Title>Login</Title>
                    </Body>
                    <Right>
                        <Button transparent>

                        </Button>
                    </Right>
                </Header>
                <Content>
                    <View style={{marginTop:30}}>
                    <Animatable.Text animation="pulse" easing="ease-out" iterationCount="infinite" style={{ textAlign: 'center',marginBottom:30 }}>
                        <Image source={require('../../utils/piknik_logo.png')} style={{width: 250, height:250}}/>
                    </Animatable.Text>
                    <Item regular error={this.state.isError} style={{marginLeft:20, marginRight:20, marginTop:20}}>
                        <Input style={{fontSize:14}} onChangeText={(email) => this.setState({email:email, isError:false,isLoginFail:false})} placeholder='email' />
                    </Item>
                    <Item regular error={this.state.isError} style={{marginLeft:20, marginRight:20, marginTop:20}}>
                        <Input style={{fontSize:14}} secureTextEntry={true} onChangeText={(password) => this.setState({password:password, isError:false,isLoginFail:false})} placeholder='password' />
                    </Item>
                        {
                            this.state.isLoginFail
                            ?
                        <Text style={{fontSize:12, textAlign:'center'}}>Wrong password or email</Text>
                                :null
                        }

                    <Button full light rounded style={color.primary} onPress={()=>this.onLogin()}>
                        <Text style={{color:'#FFFFFF', fontSize:12}}>Login</Text>
                    </Button>
                        <Text style={{color:'#000000', textAlign:'center'}}>--- or ---</Text>
                        <Button full light rounded style={color.loginFacebook}>
                            <Text style={{color:'#FFFFFF', fontSize:12}}>Login</Text>
                        </Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default connect()(screen_login)
