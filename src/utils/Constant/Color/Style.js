let React = require('react-native');

let {
    StyleSheet,
} = React;

module.exports = StyleSheet.create({
    bg : {
        backgroundColor:'#FFFFFF'
    },

    headerLognRegis:{
        backgroundColor: '#22a7f0',
    },
    loginFacebook:{
        marginTop:10,
        marginLeft:20,
        marginRight:20,
        backgroundColor:'#3b5998'
    },
    primary: {
        marginTop:10,
        marginLeft:20,
        marginRight:20,

        backgroundColor: '#22a7f0',
    }

});